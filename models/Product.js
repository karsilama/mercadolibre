var Product = function (
        id,
        title,
        picture,
        condition,
        free_shipping,
        price,
        sold_quantity,
        description,
        pictures,
        state_name
    )
{
    this.id = id;
    this.title = title;
    this.picture = picture;
    this.condition = condition;
    this.free_shipping = free_shipping;
    this.price = price;
    this.sold_quantity = isNaN(sold_quantity) ? null : sold_quantity
    this.description = description || null
    this.state_name = state_name || null
    this.pictures = pictures || null
}

module.exports = Product;
