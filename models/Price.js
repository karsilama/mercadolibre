var Price = function (
        currency,
        ammount,
        decimals
    )
{
    this.currency = currency;
    this.ammount = ammount;
    this.decimals = decimals;
}

module.exports = Price;
