## React app

Autor: Jesus Colavita


## Herramientas

* UI: React. create-react-app, typescript. SASS Preprocesador
* API: Nodejs. express, axios


### Entorno

Instalar los modulos de la api y del cliente en: ./ y ./client

```
$ cd mercadolibre
$ npm i
$ cd client && npm i
```

Iniciar la API en el root ./

```
$ npm start
```

Iniciar el dev-server del cliente ./client

```
$ cd client && npm start
```

### Páginas

La app consta de 3 componentes principales en ./src/app/pages. Se utilizó para el cliente: HTML, React y SASS y para el servidor: Node js, Expresss y Axios para las peticiones externas.


### Usabilidad y Performance

Se respetó el diseño de 12 columnas. Para resolverlo se usó grid CSS y flex model logrando un diseño fluido adaptable a cualquier dispositivo. Los tamaños de imagenes, fuentes, calles y margenes se respetaron.


### SEO

Se pueden copiar y pegar urls con los parametros indicados y mantener la navegabilidad.


### Escalabilidad

Se siguieron tutoriales y cursos cortos sobre las tecnologias, tratando de respetar buenas prácticas y patrones de diseño.

* SASS almacena variables globales y mixins para la reutilizacion de codigo.
* Se uso composición con clases para resolver la herencia.
* También se usaron componentes funcionales para objetos con pocas responsabilidades.
* Se tuvo en cuenta el concepto "Render / State changes / Re-Render" para interactuar con el Dom.
