var express = require('express');
var router = express.Router();
var axios = require('axios');

var Price = require('../models/Price');
var Product = require('../models/Product');
var Response = require('../models/Response');
var { API_URL, ROUTES } = require('../api.configs');

router.get('/?search', function(req, res, next){
    console.log('\n', '⚡', 'Route: products','⚡','\n',)

    var q = req.query.q;
    var response = new Response();

    // fetch data
    var url = API_URL + ROUTES.SEARCH + 'q=' + q;
    console.log('Endpoint', API_URL);
    console.log('Route', ROUTES.SEARCH, '\n');

    axios.get(url)
    .then(function(respuesta){

        response.categories = [];
        response.items = [];

        // ADDING PRODUCTS
        respuesta.data.results.forEach(function(item){
            var newItem = new Product(
                item.id,
                item.title,
                item.thumbnail,
                item.condition,
                item.shipping.free_shipping,
                new Price(
                    item.currency_id,
                    item.price,
                    null
                ),
                null,
                null,
                null,
                item.address.state_name
            );
            response.items.push(newItem);
        });

        // ADDING CATEGORIES
        respuesta.data.filters.forEach(function(item){
            item.values.forEach(function(subitem){

                // child
                response.categories.push(subitem);

                // // parents
                if(subitem.path_from_root){
                    subitem.path_from_root.forEach(function(from){
                        if(from.id !== subitem.id) {
                            response.categories.push(from)
                        }
                    });
                }
            })
        });

        response.categories = response.categories.reverse();

        res.json(response);
    })
    .catch(function(e) {
        console.log('Error ocurred!',e)
    })

});

module.exports = router;
