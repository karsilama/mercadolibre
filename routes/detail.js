var express = require('express');
var router = express.Router();
var axios = require('axios');

var Price = require('../models/Price');
var Product = require('../models/Product');
var Response = require('../models/Response');
var { API_URL } = require('../api.configs');

router.get('/:id', function(req, res, next){
    console.log('\n', '⚡','Route: detail','⚡','\n',)

    var id = req.params.id;

    var response = new Response();

    // fetch data
    var url = API_URL + '/items/' + id;
    var urlDescription = url + '/description';

    console.log('API_URL', API_URL);

    axios.get(urlDescription)
    .then(function(resDescription){

        var description = resDescription.data.plain_text;

        axios.get(url)
        .then(function(result){

            var item = result.data;

            // INTERCEPTANDO DATA Y PARSEO

            var price = new Price(
                item.currency_id,
                item.price,
                null
            );

            var newItem = new Product(
                item.id,
                item.title,
                item.thumbnail,
                item.condition,
                item.shipping.free_shipping,
                price,
                item.sold_quantity,
                description,
                item.pictures
            );

            response.item = newItem;

            res.json(response);
        })
        .catch(console.log)
    })
    .catch(console.log)
});

module.exports = router;
