import React from "react";
import Format from "../Format.tsx/Format";

import './ProductDetail.scss';
import Gallery from "../gallery/Gallery";

interface IProductProps {
    product: any
}

export const ProductDetail = (props:IProductProps) => {

    return(
        <div className="detail-container">
            <div className="product-images">
                <Gallery pictures={props.product.pictures} />
            </div>

            <div className="aside">
                <div className="ranking">
                    <span>
                        {props.product.condition === 'new' ? 'Nuevo' : 'Usado'}
                    </span>
                    <span>
                        {props.product.sold_quantity} Vendidos
                    </span>
                </div>
                <div className="title">
                    {props.product.title}
                </div>
                <div className="price">
                    <Format price={props.product.price} />
                </div>
                <button type="button" className="submit">
                    Comprar
                </button>
            </div>

            <div className="product-description">
                <div className="description-title">
                    Descripcion del producto
                </div>
                <div className="description-text">
                    {props.product.description}
                </div>
            </div>
        </div>
    )
}
