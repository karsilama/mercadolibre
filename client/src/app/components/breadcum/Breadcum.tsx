import React, {Component} from 'react';
import './Breadcum.scss';
import {FaChevronRight} from 'react-icons/fa';
import { Link } from 'react-router-dom';
import { QUERY_PARAMS } from '../../constants/router.const';
import { Category } from '../../constants/products.const';

interface IBreadcumProps {
    categories: Category[]
}

class Breadcum extends Component<IBreadcumProps> {
    state = { categories : [] };

    componentDidMount() {
        this.setState({
            categories: this.props.categories
        })
    }

    render() {
        return (
            <ul className="flex flex-start align-items-center breadcum">
            {
                this.state.categories.map((item: Category) =>
                <li
                    key={item.id}
                    className="link flex align-items-center">
                    <Link to={`${QUERY_PARAMS.SEARCH_PREFIX}${item.name}`}>
                        <div className="link image">
                            {item.name}
                        </div>
                        <FaChevronRight />
                    </Link>
                </li>
            )
        }
        </ul>
    )
}
}
export default Breadcum;
