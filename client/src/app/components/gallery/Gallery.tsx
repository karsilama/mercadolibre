import React, {Component} from 'react';

import './Gallery.scss';
import { IImage } from './Image';

interface GalleryProps {
    pictures: any[]
}

interface GaleryState {
    images: JSX.Element[],
    nameClasses: string[]
}

class Gallery extends Component<GalleryProps, GaleryState> {
    className = 'product-image';
    counter = 0;
    gallery: any;

    constructor(props: GalleryProps){
        super(props)
        this.state = {
            images: [],
            nameClasses: []
        }
    }

    // lyfe cicle component
    componentDidMount() {

        // push to state name classes
        let nameClasses :string[] = [];
        this.props.pictures.forEach((p, i) => {
          nameClasses.push(this.className);
        });

        // update states
        this.setState({
          nameClasses: nameClasses
        }, () => {
          this.updateImagesState();
        });
    }

    componentWillUnmount() {
        clearTimeout(this.gallery);
    }

    updateImagesState() {

        // updating JSX elemnts
        let images: JSX.Element[] = [];
        this.props.pictures.forEach((p, i) => {
            images.push(
                <img
                key={p.id}
                className={this.state.nameClasses[i]}
                src={this.props.pictures[i].url} alt="logo" />
            )
        })

        this.setState({
            images: images
        });

        this.slide();
    }

    // updating name clases to slide
    slide() {
        this.gallery = setTimeout( () => {

            // reset all
            let nameClasses = [];
            this.props.pictures.forEach((p: IImage, i: number) => nameClasses[i] = this.className);

            //set current active
            const className = `active ${this.className}`;
            nameClasses[this.counter] = className;

            // update estate name classes
            this.setState({
                nameClasses: nameClasses
            }, () => {

                // increace counter
                this.counter++;
                if(this.counter === this.props.pictures.length - 1){
                    this.counter = 0;
                }

                // infinite slide
                this.updateImagesState();
            })

        }, 1500 )
    }

    render () {
        return (
            <div className="gallery">
                <div className="images-container">
                    {this.state.images}
                </div>
            </div>
        )
    }
}


export default Gallery;
