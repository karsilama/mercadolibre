import React, { Component } from "react";

interface Price {
    currency:string;
    currency_id: string;
    decimals: number;
}

interface FormatProps {
    price: any;
}

// FORMAT STRATEGY @todo traer de /api
const currency: any = {
    'ARS' : '$'
}

export const Format = (props : FormatProps) => {

    const ammount = new Intl.NumberFormat().format(props.price.ammount);
    const simbol = currency[props.price.currency];

    return (
        <div>
        {`${simbol} ${ammount}`}
        </div>
    )
}


export default Format;
