import React from "react";

import { IProductResult } from "../product-result/ProductResult.interface"
import ProductResult from "../product-result/ProductResult";

import './ProductsList.scss';

export const ProductsList = (props:any) => {
    return(
        props.products.map((p: IProductResult) => {
            return (
                <div
                key={p.id}
                className="item-product flex flex-column justify-content-between align-items-top">
                <ProductResult
                redirectTo={`/items/${p.id}`}
                product={p} />
                </div>
            )
        })
    )
}
