import React from 'react';

import './SearchBox.scss';
import { FaSearch } from 'react-icons/fa';

interface ISearchBoxState {
    searchKey: string;
}

interface ISearchBoxProps {
    handlerRedirect: (key: string) => void
}

const defaultPlaceholder = "Nunca dejes de buscar";

class SearchBox extends React.Component<ISearchBoxProps, ISearchBoxState> {

    placeholder = defaultPlaceholder;

    constructor(props:ISearchBoxProps) {
        super(props);
        this.state = {
            searchKey: ''
        }
    }

    handlerSubmitForm(e: React.FormEvent<HTMLFormElement>) : void {
        e.preventDefault();

        // redirect to page results /items?search=
        this.props.handlerRedirect(this.state.searchKey);
    }

    handlerOnChangeInput (e: React.ChangeEvent<HTMLInputElement>) : void {
        const {value} = e.target;
        this.setState({
            searchKey: value
        });
    }

    render () {

        return (
            <div className="form">
                <form
                onSubmit={ e => this.handlerSubmitForm(e) }
                className="grow-item flex justify-content-between align-items-center">

                    <input
                    type="text"
                    autoFocus
                    name="search"
                    onChange={ e => this.handlerOnChangeInput(e) }
                    value={this.state.searchKey}
                    className="input"
                    placeholder={this.placeholder} />

                    <button
                    type="submit"
                    disabled={this.state.searchKey.length < 3}
                    className="search-button">
                    <FaSearch />
                    </button>
                </form>
            </div>
        )
    }
}

export default SearchBox;
