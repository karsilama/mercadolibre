import React from 'react';
import './Header.scss';
import logo from '../../../assets/images/logo.png';
import { Link } from 'react-router-dom';

export const Header = (props:any) => {

  return (
    <div className="header-container">
      <Link to="/" className="logo">
        <img src={logo} className="logo" alt="logo"/>
      </Link>
      <div className="header-content">
        {props.children}
      </div>
    </div>
  )
}

export default Header
