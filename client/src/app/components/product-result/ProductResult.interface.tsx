export interface Price {
    concurrency:string;
    currency_id: string;
    decimals:string;
}


export interface IProductResult {
  id: string;
  title: string;
  picture: string;
  pictures: any[];
  condition: string;
  free_shipping: string;
  price: Price;
  state_name: string
}
