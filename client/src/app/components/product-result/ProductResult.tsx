import React from 'react';
import parser from 'html-react-parser';

import './ProductResult.scss';

import {FaTruck} from 'react-icons/fa';
import { IProductResult } from './ProductResult.interface';
import { Link } from 'react-router-dom';
import Format from '../Format.tsx/Format';

type IProductResultProps = {
  redirectTo: string,
  product: IProductResult
}

export const ProductResult = (props: IProductResultProps) => {

  return (
    <div className="separator">
      <div className="flex justify-content-between item">

        <Link
          className="link image"
          to={props.redirectTo}>
          <img src={props.product.picture} alt="thumb" />
        </Link>

        <div className="flex justify-content-between grow-item info">
          <div className="flex flex-column grow-item">

            <div className="price flex align-items-center">
              <Format price={props.product.price} />

              <i className="truck">
                <FaTruck />
              </i>
            </div>

            <div className="title">
              {parser(props.product.title)}
            </div>
          </div>

          <div className="location flex align-self-end">
            {props.product.state_name}
          </div>
        </div>
      </div>
    </div>
  )
}

export default ProductResult;
