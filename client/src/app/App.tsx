import React, { Component } from 'react';
import Routes from './Routes';

import './App.scss';

class App extends Component {
    constructor(props: any) {
        super(props);
    }
    render() {
        return (
            <div className="grid">
                <Routes />
            </div>
        )
    }
}

export default App
