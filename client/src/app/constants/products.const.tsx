export const PRODUCT_PRICE_SIMBOLS = {
    'ARS' : '$'
}

export interface Category {
    id: string;
    name: string;
}
