import React, { Component } from 'react';
import { Route } from 'react-router';

import Detail from './pages/detail/Detail';
import Search from './pages/search/Search';
import Main from './pages/main/Main';

class Routes extends Component {
    render(){
        return(
            <>
                <Route path="/" component={Main} />
                <Route exact path="/items" component={Search} />
                <Route path="/items/:id" component={Detail} />
            </>
        )
    }
}


export default Routes;
