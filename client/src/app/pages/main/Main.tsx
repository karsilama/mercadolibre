import React, {Component} from 'react';
import Header from '../../components/header/Header';
import SearchBox from '../../components/search-box/SearchBox';
import { QUERY_PARAMS } from '../../constants/router.const';

class Main extends Component<any> {
    state = {reset:false}
    // on search, on press breadcum item
    handlerRedirect(key: string) {
        this.props.history.push(`${QUERY_PARAMS.SEARCH_PREFIX}${key}`)
    }

    render() {
        return(
            <>
                <div className="header">
                    <Header>
                        <SearchBox handlerRedirect={this.handlerRedirect.bind(this)}/>
                    </Header>
                </div>
            </>
        )
    }
}

export default Main;
