import React, {Component} from 'react';
import { matchPath } from 'react-router';
import ReactLoading from 'react-loading';

import './Detail.scss';

import { IProductResult } from '../../components/product-result/ProductResult.interface';
import { ProductDetail } from '../../components/product-detail/ProductDetail';

interface IDetailState {
    loading: boolean;
    product: IProductResult | null
}

interface IDeatilProps {}

class Detail extends Component<any, IDetailState> {

    constructor(props: IDeatilProps){
        super(props);
        this.state = {
            loading: true,
            product: null
        }
    }

    // getting params from location & matchPath
    getIdFromParams() {
        const match: any = matchPath(this.props.location.pathname, {
            path: '/items/:id',
            exact: true,
            strict: false
        });
        return match.params.id;
    }

    // calling api
    fetchData(url: string): Promise<any> {
        return new Promise( (resolve: any) => {
            fetch(url)
            .then(res => res.json())
            .then(data => {
                resolve(data)
            })
        })
    }

    //  getting product & description: life cicle component
    componentDidMount() {
        const id = this.getIdFromParams();
        this.fetchResults(id);
    }

    fetchResults(id: string) {
        const product = `/api/items/${id}`;
        this.fetchData(product).then(res => {
            this.setState({
                product: res.item
            }, () => {
                this.setState({
                    loading: false
                })
            });
        })
    }

    render() {

        // reder > state change > re render

        if(this.state.loading){
            return (
                <ReactLoading
                className="loading"
                color="#ccc"
                height={90}
                width={100} />
            )
        }

        else {
            return (
                <div className="main">
                    <ProductDetail
                        product={this.state.product}  />
                </div>
            )
        }

    }
}

export default Detail;
