import React, {Component} from 'react';
import ReactLoading from 'react-loading';
import queryString from 'query-string';
import { Category } from '../../constants/products.const';

import './Search.scss';

import { ProductsList } from '../../components/products-list/ProductsList';
import Breadcum from '../../components/breadcum/Breadcum';

interface IResultsState {
    loading: boolean;
    categories: Category[];
    products: JSX.Element[];
}

class Search extends Component<any, IResultsState> {
    storageSearch: string | null = null;

    constructor(props: any) {
        super(props);
        this.state = {
            loading: true,
            products: [],
            categories: [],
        }
    }

    // gettin paramas from location using queryString
    fetch() {
        const params: any = queryString.parse(this.props.location.search);
        console.log(params)
        if(params.search.length > 0){
            this.fetchResults(params.search);
        }
    }

    componentDidMount() {
        this.fetch();
    }

    // fetching from api
    fetchResults(text: string) : void {
        this.setState({
            loading: true
        },  ()=>{

            let url = `/api/items/search?q=${text}`

            fetch(url)
            .then(res => res.json())
            .then(data => {
                if(data && data.items){
                    this.setState({
                        products: data.items.splice(0, 4),
                        categories: data.categories
                    }, () => {
                        this.setState({
                            loading: false
                        })
                    });
                }
            })
        })
    }

    // on location hash changed fetch again
    componentDidUpdate(prevProps:any, nextState:any) {
        if(this.props.location.search !== prevProps.location.search) {
            this.fetch();
        }
    }

    render() {

        if(this.state.loading) {
            return (
                <ReactLoading
                className="loading"
                color="#ccc"
                height={90}
                width={100} />
            )
        }

        else {
            if(this.state.products.length > 0){
                return (
                    <>
                        <div className="breadcum">
                            <Breadcum categories={this.state.categories} />
                        </div>
                        <div className="main">
                            <div className="flex flex-column justify-content-between align-items-top">
                                <ProductsList products={this.state.products} />
                            </div>
                        </div>
                    </>
                )
            } else {
                return (
                    <div className="main">
                    Sin resultados
                    </div>
                )
            }
        }
    }
}

export default Search;
