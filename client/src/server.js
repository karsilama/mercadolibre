const express = require('express');
const bodyParser = require('body-parser');
const queryString = require('querystring');
// const axios = require('axios');
const morgan = require('morgan');

const app = express();
app.set('port', process.env.PORT || 3001);
const port = app.get('port');

// middleware
app.use(morgan('dev'));
app.use(express.json());


app.listen(port, () => {
    console.log('Server linstening on port ' + port);
});

// axios.get('https://api.mercadolibre.com/sites/MLA')
