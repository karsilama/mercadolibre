const express = require('express');
const router = express.Router();

router.get('/items?search=', (req, resp) => {
    console.log('Searching items math with', req.query.search);
    res.json({
        'status': 'OK',
        'q': req.query.search
    })
})

module.exports = router;
